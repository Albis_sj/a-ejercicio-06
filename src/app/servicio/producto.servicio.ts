import { Injectable } from "@angular/core";

@Injectable ()

export class ProductosServicio {

  public productos: producto[] = [
    {
      nombre: 'Television',
      descripcion: 'La TV ipsum dolor sit amet consectetur adipisicing elit. Impedit est eaque laudantium sapiente numquam quaerat earum asperiores quae voluptates dolor iusto eum ipsa, accusantium inventore voluptatem aliquid laborum. Recusandae, dolore.',
      img: 'assets/img/tv.jpg' 
    },
    {
      nombre: 'Teclado',
      descripcion: 'El teclado ipsum dolor sit amet consectetur adipisicing elit. Impedit est eaque laudantium sapiente numquam quaerat earum asperiores quae voluptates dolor iusto eum ipsa, accusantium inventore voluptatem aliquid laborum. Recusandae, dolore.',
      img: 'assets/img/teclado.jpg'
    },
    {
      nombre: 'Lavadora',
      descripcion: 'La lavadora ipsum dolor sit amet consectetur adipisicing elit. Impedit est eaque laudantium sapiente numquam quaerat earum asperiores quae voluptates dolor iusto eum ipsa, accusantium inventore voluptatem aliquid laborum. Recusandae, dolore.',
      img: 'assets/img/lavadora.jpg' 
    },
    {
      nombre: 'Comedor',
      descripcion: 'El comedor ipsum dolor sit amet consectetur adipisicing elit. Impedit est eaque laudantium sapiente numquam quaerat earum asperiores quae voluptates dolor iusto eum ipsa, accusantium inventore voluptatem aliquid laborum. Recusandae, dolore.',
      img: 'assets/img/comedor.jpg'
    },
    {
      nombre: 'Adornos',
      descripcion: 'Los adornos ipsum dolor sit amet consectetur adipisicing elit. Impedit est eaque laudantium sapiente numquam quaerat earum asperiores quae voluptates dolor iusto eum ipsa, accusantium inventore voluptatem aliquid laborum. Recusandae, dolore.',
      img: 'assets/img/adornos.jpg'
    }
  ];

  constructor(){
    console.log('servicio');
    
  }

getProductos():any[]{
  return this.productos;
}

}


export interface producto { 
  nombre: string;
  descripcion: string;
  img: string;
}