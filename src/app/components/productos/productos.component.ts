import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductosServicio } from '../../servicio/producto.servicio';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  productos: any[] = [];
  teclado!: any;
  tv!: any;
  lavadora!: any;
  comedor!: any;
  adornos!: any;

  constructor(private activatedRoute: ActivatedRoute,
            private _productoServicio: ProductosServicio) { 
  console.log('constructor productos');
  this.productos = this._productoServicio.getProductos();
  console.log(this.productos);
  this.teclado = this.productos[1];
  console.log(this.teclado);
  this.tv = this.productos[0];
  this.lavadora = this.productos[2];
  this.comedor = this.productos[3];
  this.adornos = this.productos[4];
  
  
  
  }

  ngOnInit(): void {
    console.log('ngOnInit productos');
    
  }
}